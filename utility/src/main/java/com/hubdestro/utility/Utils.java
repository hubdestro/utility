package com.hubdestro.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Demonstrate on 11/4/16
 */
public class Utils {

    //fields declaration
    public static boolean DEBUG_MODE = true;

    /***
     *
     * @return Object of Context Class
     */
    private static Context getContext() {
        String className = Thread.currentThread().getStackTrace()[3].getClassName()+".class";
        Class c ;
        Context context = null;
        try {
            c = Class.forName(className);
            Object o = c.newInstance();
            context = (Context)o;
        }catch (Exception e ) {
            e.printStackTrace();
        }
        return context;
    }

    /***
     * to print debug into logcat
     * @param msg Receive a String to print in to logcat
     */
    public static void d(String msg) {
        if(DEBUG_MODE && msg != null) {
            Log.d(Thread.currentThread().getStackTrace()[3].getMethodName(),msg);
        }
    }

    /***
     * to print warning into logcat
     * @param msg Receive a String to print in to logcat
     */
    public static void w(String msg) {
        if(DEBUG_MODE && msg != null) {
            Log.w(Thread.currentThread().getStackTrace()[3].getMethodName(),msg);
        }
    }

    /***
     * to print information into logcat
     * @param msg Receive a String to print in to logcat
     */
    public static void i(String msg) {
        if(DEBUG_MODE && msg != null) {
            Log.i(Thread.currentThread().getStackTrace()[3].getMethodName(),msg);
        }
    }

    /***
     * to print verbose into logcat
     * @param msg Receive a String to print in to logcat
     */
    public static void v(String msg) {
        if(DEBUG_MODE && msg != null) {
            Log.v(Thread.currentThread().getStackTrace()[3].getMethodName(),msg);
        }
    }

    /***
     *
     * @param msg Receive a msg to display in your Toast
     */
    public static void showToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /***
     *
     * @param textView Receive TextView or EditText
     * @return it will return trim string from that widgets
     */
    public static String getStringFrom(TextView textView) {
        return  textView != null ? textView.getText().toString().trim() : " ";
    }

    /***
     *
     * @param msg Receive a msg to print into Alert Dialog
     */
    public static void showMessageDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Message");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static AlertDialog.Builder showMessageDialog(Context context, String message) {
        AlertDialog.Builder builder = null;
        builder = new AlertDialog.Builder(context);
        builder.setTitle("Message");
        builder.setMessage(message);
        return builder;
    }

    /***
     *
     * @return return genrated Device Id(IMEI NO)
     */
    public static String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    /***
     *
     * @return true if network available otherwise false
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

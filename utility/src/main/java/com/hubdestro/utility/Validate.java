package com.hubdestro.utility;

import android.text.TextUtils;
import android.util.Patterns;
import android.widget.TextView;
import java.util.regex.Pattern;

/**
 * Created by root on 12/5/16
 */
public class Validate {

    /***
     * ^ - start of string
     * ?=. -  representing at least one
     * ?=\\s+$ - no white space till the end
     * $ - represent end of regexp
     */
    private static final String PASSWORD_PATTERN =
            "^(?=.*[a-z])(?=\\S+$).{8,}$";
    //"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";


    /***
     * This method will validate email Address
     * with requirement there should be at(@) symbol in email and there should be dot(.) and
     * there should be few character in between @ and . symbol
     * and after the @ symbol you can not use any other special char except .(dot)
     */
    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /***
     * This method will validate password with minimum requirement
     * Validation would work according to pattern
     */
    public static boolean isValidPassword(String password) {
        return Pattern.compile(PASSWORD_PATTERN).matcher(password).matches();
    }

    //to validate phone no
    //must be 10 digit long
    public static boolean isValidPhone(String phone) {
        return phone.matches("[7-9][0-9]{9}");
    }

    /***
     * to check component is empty or not
     * @param field instance of TextView and its Child Class
     * @return true if component is empty otherwise false
     */
    public static boolean isEmpty(TextView field) {
        return field != null && TextUtils.isEmpty(field.getText().toString());
    }

    /***
     * to check multiple component are empty or not
     * @param fields array of component
     * @return true if one of them is empty otherwise false
     */
    public static boolean isAllEmpty(TextView... fields) {
        boolean status = isEmpty(fields[0]);
        for (int i = 1; i < fields.length; i++) {
            status = status || isEmpty(fields[i]);
        }
        return status;
    }

}

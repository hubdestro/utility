# README #

### UTILITY FOR ANDROID DEVELOPER ###

* Common methods
* 2.0
* [Learn More](https://hubdestro.blogspot.in/)

### Whats New ###
New Validation class added in version 2.0

### How do I get set up? ###

* Maven Dependency

```
<dependency>
  <groupId>com.hubdestro</groupId>
  <artifactId>utility</artifactId>
  <version>1.0</version>
  <type>pom</type>
</dependency>
```

* Gradle Dependency


```
#!python

compile 'com.hubdestro:utility:1.0'
```


we are working more on this utility library its not available on jCenter, so please add this in your gradle repository:


```
allprojects {
    repositories {
        jcenter()
        maven {
            url 'https://dl.bintray.com/hubdestro/Hubdestro-Labs'
       }
    }
}
```


### Contribution guidelines ###

* Ankit Demonstrate
     
###HUBDESTRO LABS ###


Visit for more [http://hubdestro.blogspot.in]